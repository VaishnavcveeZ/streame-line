# Streamline

Streamline is a Go package designed to simplify the creation and management of WebSocket servers. It provides a robust framework for handling real-time communication between clients and a server, with features such as rate limiting, dynamic message filtering, and JWT authentication.

## Features

- **WebSocket Handling**: Streamline handles the complexities of WebSocket connections, including the upgrade process from HTTP to WebSocket.
- **Rate Limiting**: Protect your server from abuse by implementing rate limiting to control the rate of incoming requests.
- **Dynamic Message Filtering**: Add or remove filters at runtime to control which messages are processed by the server.
- **JWT Authentication**: Secure your WebSocket connections by validating JWT tokens and associating them with user sessions.
- **Message Queue**: Efficiently process messages in a queue, ensuring that no messages are lost during high load periods.
- **Graceful Shutdown**: Listen for operating system signals to gracefully shut down the server, ensuring all resources are cleaned up.
- **External API Integration**: Forward messages to an external API for further processing or storage.

## Installation

To install the Streamline package, use the following command:

sh go get github.com/yourusername/streamline


Replace `github.com/yourusername/streamline` with the actual path to your Streamline package.

## Usage

To use Streamline, you'll need to create an instance of the `StreamlineAPI` interface and configure it according to your needs.

go import ( "github.com/yourusername/streamline" )

func main() { // Create a new Streamline instance with a JWT validator and API URL s := streamline.NewStreamline(jwtValidator, "https://api.example.com", 100)

// Enable rate limiting
s.EnableRateLimiter()
// Add a dynamic filter
s.AddFilter(streamline.NewEventTypeFilter("exampleEventType", "exampleFilterID"))
// Start listening for shutdown signals
go s.ListenForShutdownSignals()
// Start the HTTP server
http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
	s.UpgradeToWebSocket(w, r)
})
http.ListenAndServe(":8080", nil)
}


### JWT Validation

To validate JWT tokens, you can set a custom JWT validator function:

go func myJWTValidator(tokenString string) (string, error) { // Implement your JWT validation logic here return "", nil }

s.SetJWTValidator(myJWTValidator)


### Rate Limiting

To enable rate limiting with a custom configuration, you can use the `EnableRateLimiter` method:

go s.EnableRateLimiter()


### Dynamic Message Filtering

To add or remove dynamic filters, use the `AddFilter` and `RemoveFilter` methods:

go // Add a filter s.AddFilter(streamline.NewEventTypeFilter("exampleEventType", "exampleFilterID"))

// Remove a filter s.RemoveFilter("exampleFilterID")


### Broadcasting Messages

To broadcast a message to all connected users, use the `BroadcastMessage` method:

go func broadcastHandler(w http.ResponseWriter, r *http.Request) { s.BroadcastMessage(w, r) }


### External API Integration

To forward messages to an external API, you can use the `forwardMessageToAPI` method:

go func (s *streamline) forwardMessageToAPI(msg message) error { // Implement your API forwarding logic here return nil }


## Contributing

Contributions are welcome! Please feel free to submit pull requests or open issues for any bugs or feature requests.
