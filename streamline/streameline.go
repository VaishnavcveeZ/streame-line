package streamline

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

// userSession represents a user session.
type userSession struct {
	id       string
	Conn     *websocket.Conn
	SendChan chan []byte
}

// message represents a message to be sent over WebSocket.
type message struct {
	EventType string      `json:"eventType"`
	Content   interface{} `json:"content"`
	Receiver  []string    `json:"receiver"`
	Sender    string      `json:"sender"`
}

// DynamicFilter represents a dynamic filter that can be added or removed at runtime.
type DynamicFilter interface {
	Match(msg message) bool
	ID() string // Unique identifier for the filter
}

// EventTypeFilter is a simple filter that matches messages based on event type.
type EventTypeFilter struct {
	EventType string
	filterID  string // Unique identifier for the filter
}

func (f *EventTypeFilter) Match(msg message) bool {
	return msg.EventType == f.EventType
}

// Add the ID method to the EventTypeFilter struct
func (f *EventTypeFilter) ID() string {
	return f.filterID
}

// Set the filter ID when creating a new EventTypeFilter
func NewEventTypeFilter(eventType string, filterID string) *EventTypeFilter {
	return &EventTypeFilter{
		EventType: eventType,
		filterID:  filterID,
	}
}

// RateLimiter is a simple rate limiter that uses a token bucket algorithm.
type RateLimiter struct {
	tokens     int
	maxTokens  int
	refill     time.Duration
	lastRefill time.Time
	mu         sync.Mutex
}

// NewRateLimiter creates a new rate limiter with the specified maximum number of tokens,
// the refill rate, and the initial number of tokens.
func NewRateLimiter(maxTokens int, refill time.Duration, initialTokens int) *RateLimiter {
	return &RateLimiter{
		tokens:     initialTokens,
		maxTokens:  maxTokens,
		refill:     refill,
		lastRefill: time.Now(),
	}
}

// TakeToken attempts to take a token from the rate limiter. If a token is available, it returns true.
// Otherwise, it returns false.
func (rl *RateLimiter) TakeToken() bool {
	rl.mu.Lock()
	defer rl.mu.Unlock()

	// Refill tokens if necessary
	now := time.Now()
	if now.Sub(rl.lastRefill) >= rl.refill {
		rl.tokens = rl.maxTokens
		rl.lastRefill = now
	}

	// Check if a token is available
	if rl.tokens > 0 {
		rl.tokens--
		return true
	}

	return false
}

// JWTValidator is a function type that validates JWT tokens and returns the user's unique ID.
type JWTValidator func(tokenString string) (string, error)

// StreamlineAPI defines the interface for interacting with the Streamline package.
// It includes methods for serving HTTP requests, upgrading them to WebSocket connections, and broadcasting messages.
// StreamlineAPI defines the interface for interacting with the Streamline package.
// It includes methods for serving HTTP requests, upgrading them to WebSocket connections, and broadcasting messages.
type StreamlineAPI interface {
	// EnableTLS enables WebSocket Secure (WSS) mode for the server.
	// This function sets the paths to the TLS certificate and key files, which are
	// used to establish secure WebSocket connections. When WSS is enabled,
	// the server will use these files to authenticate itself to clients and
	// to encrypt the data transmitted over the WebSocket connections.
	//
	// Parameters:
	// - tlsCertFile: The path to the TLS certificate file. This file is
	//   used to authenticate the server to clients.
	// - tlsKeyFile: The path to the TLS key file. This file is used
	//   to decrypt the data transmitted over the WebSocket connections.
	//
	// Example usage:
	// s := NewStreamline(...)
	// s.EnableTLS("path/to/cert.pem", "path/to/key.pem")
	//
	// This function should be called before starting the server to ensure that the
	// server is properly configured for WSS mode. If this function is not
	// called, the server will operate in the regular WebSocket mode (ws).
	EnableTLS(tlsCertFile string, tlsKeyFile string)

	// UpgradeToWebSocket upgrades the HTTP connection to a WebSocket connection.
	// It handles the WebSocket handshake and initializes a new user session.
	// The user session includes a WebSocket connection, a send channel for outgoing messages, and a unique user ID.
	//
	// Parameters:
	// - w: The HTTP response writer.
	// - r: The HTTP request.
	UpgradeToWebSocket(w http.ResponseWriter, r *http.Request)

	// BroadcastMessage broadcasts a message to all connected users.
	// This function parses the request body, prepares the message to be broadcasted,
	// and sends it to all active user sessions. The message is sent to each user's
	// WebSocket connection, allowing for real-time communication with all clients.
	//
	// The function handles the creation of the message, the serialization of the message,
	// and the sending of the message to each user's WebSocket connection. If an error
	// occurs during message serialization or sending, the function logs the error
	// and continues broadcasting the message to the remaining users.
	//
	// Example usage:
	// s := NewStreamline(...)
	// // Assume an HTTP request is received with a message to be broadcasted
	// s.BroadcastMessage(w, r)
	//
	// This function is called in response to an HTTP request to broadcast a message
	// to all connected users. It is also used in the message processing pipeline to send messages
	// to all users.
	BroadcastMessage(w http.ResponseWriter, r *http.Request)

	// EnableRateLimiter initializes and enables the rate limiter for the streamline instance.
	// This method should be called during the setup phase of the streamline instance to
	// configure the rate limiting behavior. The rate limiter is used to control the
	// rate at which requests are processed, preventing abuse and ensuring fair
	// usage of the service.
	//
	// The rate limiter is based on a token bucket algorithm, where each incoming request
	// consumes a token. If the bucket is empty, the request is denied, and the client
	// is informed that the rate limit has been exceeded.
	//
	// The rate limiter is configured with a maximum number of tokens, a refill rate, and
	// an initial number of tokens. The maximum number of tokens represents the burst
	// capacity, allowing for short bursts of requests. The refill rate determines how often
	// new tokens are added to the bucket, effectively controlling the average rate of
	// requests. The initial number of tokens is the starting capacity of the bucket.
	//
	// Example usage:
	// s := NewStreamline(...)
	// s.EnableRateLimiter() // Enable rate limiting with default configuration
	//
	// Note: The default configuration may not be suitable for all use cases. You should
	// adjust the parameters of the rate limiter to match the expected load and desired
	// rate limiting behavior for your application.
	EnableRateLimiter()

	// AddFilter adds a dynamic filter to the streamline instance.
	// This function appends the provided filter to the dynamicFilters slice,
	// allowing the filter to be applied to incoming messages.
	//
	// Parameters:
	// - filter: The DynamicFilter to be added. It must implement the Match and ID methods.
	//
	// Example usage:
	// s := NewStreamline(...)
	// s.AddFilter(&EventTypeFilter{EventType: "example"})
	AddFilter(filter DynamicFilter)

	// RemoveFilter removes a dynamic filter from the streamline instance by its ID.
	// This function searches the dynamicFilters slice for a filter with the specified ID
	// and removes it if found. This effectively stops the filter from being applied to
	// incoming messages.
	//
	// Parameters:
	// - filterID: The unique identifier of the filter to be removed.
	//
	// Example usage:
	// s := NewStreamline(...)
	// s.RemoveFilter("exampleFilterID")
	RemoveFilter(filterID string)

	// ListenForShutdownSignals listens for operating system signals that indicate the server
	// should shut down gracefully. This function is crucial for ensuring that the server can
	// clean up resources and close connections properly before exiting. It prevents data loss
	// or corruption by allowing the server to perform necessary cleanup tasks.
	//
	// This function creates a channel to receive operating system signals (SIGINT and SIGTERM)
	// and blocks until one of these signals is received. Upon receiving a signal, it calls the
	// shutdown method to initiate the graceful shutdown process.
	//
	// The shutdown process typically involves closing the shutdown signal channel to signal
	// other parts of the application that shutdown is in progress, closing the message queue
	// to stop processing new messages, locking the hub to prevent new connections, and closing
	// all active WebSocket connections.
	//
	// Example usage:
	// s := NewStreamline(...)
	// go s.ListenForShutdownSignals() // Start listening for shutdown signals in a separate goroutine
	//
	// This function should be called as a goroutine to ensure that the server can continue
	// processing other tasks while waiting for a shutdown signal.
	ListenForShutdownSignals()
}

// streamline is the main package struct that implements the streamlineAPI interface.
type streamline struct {
	upgrader             websocket.Upgrader
	hub                  map[string]*userSession
	jwtKeyFunc           JWTValidator
	dynamicFilters       []DynamicFilter // Slice to hold dynamic filters
	messageForwardApiURL string
	mu                   sync.RWMutex // Mutex for safe concurrent access
	jwtValidator         JWTValidator // Custom JWT validator function
	messageQueue         chan message
	shutdownSignal       chan struct{} // Signal channel for graceful shutdown
	rateLimiter          *RateLimiter
	tlsCertFile          string // Path to the TLS certificate file
	tlsKeyFile           string // Path to the TLS key file
}

// NewStreamline creates a new instance of the StreamlineAPI interface.
// It initializes a new streamline struct with the provided JWT key function and API URL.
// The JWT key function is used to validate JWT tokens, and the API URL is used for forwarding messages to an external API.
//
// Parameters:
// - jWTValidator: A function that validates JWT tokens string and returns the user's unique ID.
// - MessageForwardApiURL: The URL of the external API to which messages will be forwarded.
// - tlsCertFile: The path to the TLS certificate file (optional).
// - tlsKeyFile: The path to the TLS key file (optional).
//
// Returns:
// - StreamlineAPI: An interface that allows interacting with the Streamline package.
func NewStreamline(jWTValidator JWTValidator, MessageForwardApiURL string, queueSize int) StreamlineAPI {
	s := &streamline{
		upgrader:             websocket.Upgrader{},
		hub:                  make(map[string]*userSession),
		jwtKeyFunc:           jWTValidator,
		messageForwardApiURL: MessageForwardApiURL,
		messageQueue:         make(chan message, queueSize),
		shutdownSignal:       make(chan struct{}), // Initialize the shutdown signal channel
	}

	go s.processMessages()

	return s
}

// EnableTLS enables WebSocket Secure (WSS) mode for the server.
// This function sets the paths to the TLS certificate and key files, which are
// used to establish secure WebSocket connections. When WSS is enabled,
// the server will use these files to authenticate itself to clients and
// to encrypt the data transmitted over the WebSocket connections.
//
// Parameters:
//   - tlsCertFile: The path to the TLS certificate file. This file is
//     used to authenticate the server to clients.
//   - tlsKeyFile: The path to the TLS key file. This file is used
//     to decrypt the data transmitted over the WebSocket connections.
//
// Example usage:
// s := NewStreamline(...)
// s.EnableTLS("path/to/cert.pem", "path/to/key.pem")
//
// This function should be called before starting the server to ensure that the
// server is properly configured for WSS mode. If this function is not
// called, the server will operate in the regular WebSocket mode (ws).
func (s *streamline) EnableTLS(tlsCertFile string, tlsKeyFile string) {
	s.tlsCertFile = tlsCertFile
	s.tlsKeyFile = tlsKeyFile
}

// EnableRateLimiter initializes and enables the rate limiter for the streamline instance.
// This method should be called during the setup phase of the streamline instance to
// configure the rate limiting behavior. The rate limiter is used to control the
// rate at which requests are processed, preventing abuse and ensuring fair
// usage of the service.
//
// The rate limiter is based on a token bucket algorithm, where each incoming request
// consumes a token. If the bucket is empty, the request is denied, and the client
// is informed that the rate limit has been exceeded.
//
// The rate limiter is configured with a maximum number of tokens, a refill rate, and
// an initial number of tokens. The maximum number of tokens represents the burst
// capacity, allowing for short bursts of requests. The refill rate determines how often
// new tokens are added to the bucket, effectively controlling the average rate of
// requests. The initial number of tokens is the starting capacity of the bucket.
//
// Example usage:
// s := NewStreamline(...)
// s.EnableRateLimiter() // Enable rate limiting with default configuration
//
// Note: The default configuration may not be suitable for all use cases. You should
// adjust the parameters of the rate limiter to match the expected load and desired
// rate limiting behavior for your application.
func (s *streamline) EnableRateLimiter() {
	
	// Initialize the rate limiter with a default configuration.
	// The default configuration allows for 100 requests per minute, with a burst
	// capacity of 100 requests. Adjust these values as needed for your application.
	s.rateLimiter = NewRateLimiter(100, time.Minute, 100)
}

// rateLimit is a method that checks if a request should be allowed based on the rate limiter.
func (s *streamline) rateLimit(w http.ResponseWriter, r *http.Request) bool {
	if !s.rateLimiter.TakeToken() {
		http.Error(w, "Rate limit exceeded", http.StatusTooManyRequests)
		return false
	}
	return true
}

// UpgradeToWebSocket upgrades the HTTP connection to a WebSocket connection.
// It handles the WebSocket handshake and initializes a new user session.
// The user session includes a WebSocket connection, a send channel for outgoing messages, and a unique user ID.
//
// Parameters:
// - w: The HTTP response writer.
// - r: The HTTP request.
func (s *streamline) UpgradeToWebSocket(w http.ResponseWriter, r *http.Request) {

	if !s.rateLimit(w, r) {
		return // Rate limit exceeded, response has already been sent
	}

	s.upgrader.ReadBufferSize = 1024
	s.upgrader.WriteBufferSize = 1024

	s.upgrader.CheckOrigin = func(r *http.Request) bool {
		// Check the origin of the request to ensure it's from a trusted source
		return true // Replace with your own origin check logic
	}

	// Check if the request is using the 'wss' scheme
	isWSS := r.URL.Scheme == "wss"

	// If WSS is enabled, use the TLS certificate and key files
	var conn *websocket.Conn
	var err error
	if isWSS {
		s.upgrader.EnableCompression = true

		s.upgrader.Subprotocols = []string{"secure-protocol"}
		s.upgrader.Error = func(w http.ResponseWriter, r *http.Request, status int, reason error) {
			// Handle errors during the upgrade process
			http.Error(w, reason.Error(), status)
		}

		// Upgrade the HTTP connection to a WebSocket connection
		conn, err = s.upgrader.Upgrade(w, r, nil)
		if err != nil {
			http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
			return
		}
		defer conn.Close()

	} else {

		var conn *websocket.Conn
		conn, err = s.upgrader.Upgrade(w, r, nil)
		if err != nil {
			http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
			return
		}
		defer conn.Close()
	}

	// Extract JWT token from header
	tokenString := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	var userID string
	if s.jwtValidator != nil {
		userID, err = s.jwtValidator(tokenString)
		if err != nil {
			conn.WriteMessage(websocket.TextMessage, []byte("Invalid token"))
			return
		}
	} else {
		// Generate a UUID for the user ID if no custom JWT validator is provided
		userID = uuid.New().String()
	}

	userSession := &userSession{
		id:       userID,
		Conn:     conn,
		SendChan: make(chan []byte),
	}

	s.mu.Lock()
	s.hub[userID] = userSession
	s.mu.Unlock()
	defer func() {
		s.mu.Lock()
		delete(s.hub, userID)
		s.mu.Unlock()
	}()

	go s.readMessages(userSession)
	go s.writeMessages(userSession)
}

// ListenForShutdownSignals listens for operating system signals that indicate the server
// should shut down gracefully. This function is crucial for ensuring that the server can
// clean up resources and close connections properly before exiting. It prevents data loss
// or corruption by allowing the server to perform necessary cleanup tasks.
//
// This function creates a channel to receive operating system signals (SIGINT and SIGTERM)
// and blocks until one of these signals is received. Upon receiving a signal, it calls the
// shutdown method to initiate the graceful shutdown process.
//
// The shutdown process typically involves closing the shutdown signal channel to signal
// other parts of the application that shutdown is in progress, closing the message queue
// to stop processing new messages, locking the hub to prevent new connections, and closing
// all active WebSocket connections.
//
// Example usage:
// s := NewStreamline(...)
// go s.ListenForShutdownSignals() // Start listening for shutdown signals in a separate goroutine
//
// This function should be called as a goroutine to ensure that the server can continue
// processing other tasks while waiting for a shutdown signal.
func (s *streamline) ListenForShutdownSignals() {
	// Create a channel to receive operating system signals.
	sigChan := make(chan os.Signal, 1)
	// Register the channel to receive SIGINT and SIGTERM signals.
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	// Block until a signal is received.
	<-sigChan
	// Upon receiving a signal, initiate the graceful shutdown process.
	s.shutdown()
}

// shutdown initiates the graceful shutdown process of the WebSocket server.
// This function is called when the server receives a shutdown signal (SIGINT or SIGTERM).
// The goal of the shutdown process is to ensure that all resources are properly cleaned up
// and that the server can exit without causing data loss or corruption.
//
// The shutdown process involves several steps:
//  1. Close the shutdown signal channel to signal that shutdown is in progress.
//  2. Close the message queue to stop processing new messages. This prevents new messages
//     from being added to the queue while the server is shutting down.
//  3. Lock the hub to prevent new connections from being established. This ensures that
//     no new WebSocket connections are accepted during the shutdown process.
//  4. Close all active WebSocket connections. This ensures that all clients are notified
//     that the server is shutting down and that they can close their connections gracefully.
//  5. Clear the hub to release resources. This removes all user sessions from the hub,
//     freeing up memory and other resources.
//
// Example usage:
// s := NewStreamline(...)
// s.ListenForShutdownSignals() // Start listening for shutdown signals
// // Upon receiving a shutdown signal, the shutdown function is called automatically.
func (s *streamline) shutdown() {

	// Close the shutdown signal channel to signal shutdown
	close(s.shutdownSignal)

	// Close the message queue to stop processing messages
	close(s.messageQueue)

	// Lock the hub to prevent new connections
	s.mu.Lock()
	defer s.mu.Unlock()

	// Close all active WebSocket connections
	for _, userSession := range s.hub {

		if err := userSession.Conn.Close(); err != nil {
			log.Printf("Failed to close WebSocket connection for user %s: %v", userSession.id, err)
		}

		// Close the send channel to signal the writeMessages goroutine to exit
		close(userSession.SendChan)
	}

	// Clear the hub to release resources
	s.hub = make(map[string]*userSession)
}

// SetJWTValidator sets a custom JWT validator function for the streamline instance.
// This function should validate the JWT token and return the user's unique ID.
// This unique ID will be used to register the user to the WebSocket connection.
//
// Parameters:
// - validator: A function that validates JWT tokens and returns the user's unique ID.
func (s *streamline) SetJWTValidator(validator JWTValidator) {
	s.jwtValidator = validator
}

// AddFilter adds a dynamic filter to the streamline instance.
// This function appends the provided filter to the dynamicFilters slice,
// allowing the filter to be applied to incoming messages.
//
// Parameters:
// - filter: The DynamicFilter to be added. It must implement the Match and ID methods.
//
// Example usage:
// s := NewStreamline(...)
// s.AddFilter(&EventTypeFilter{EventType: "example"})
func (s *streamline) AddFilter(filter DynamicFilter) {

	// Append the new filter to the dynamicFilters slice.
	// This ensures that the filter is considered in the message processing logic.
	s.dynamicFilters = append(s.dynamicFilters, filter)
}

// RemoveFilter removes a dynamic filter from the streamline instance by its ID.
// This function searches the dynamicFilters slice for a filter with the specified ID
// and removes it if found. This effectively stops the filter from being applied to
// incoming messages.
//
// Parameters:
// - filterID: The unique identifier of the filter to be removed.
//
// Example usage:
// s := NewStreamline(...)
// s.RemoveFilter("exampleFilterID")
func (s *streamline) RemoveFilter(filterID string) {
	// Iterate over the dynamicFilters slice to find the filter with the specified ID.
	for i, filter := range s.dynamicFilters {
		// Check if the current filter's ID matches the filterID to be removed.
		if filter.ID() == filterID {
			// If a match is found, remove the filter from the slice.
			// This is done by appending all elements after the matched filter to a new slice,
			// effectively skipping the matched filter.
			s.dynamicFilters = append(s.dynamicFilters[:i], s.dynamicFilters[i+1:]...)
			// Since we've found and removed the filter, we can break out of the loop.
			break
		}
	}
}

// readMessages reads messages from the WebSocket connection associated with the given user session.
// This function continuously listens for incoming messages on the user's WebSocket connection.
// Upon receiving a message, it processes the message by applying any dynamic filters and then
// forwards the message to the message queue for further processing.
//
// The function operates in a loop, continuously reading messages until an error occurs
// (such as the WebSocket connection being closed). If an error occurs, the function breaks out
// of the loop, effectively ending the message reading process for the current user session.
//
// Dynamic filters are applied to each incoming message. If a message passes all dynamic filters,
// it is considered valid and is forwarded to the message queue. If a message fails any dynamic filter,
// it is skipped and not forwarded to the message queue.
//
// Example usage:
// userSession := &userSession{...} // Assume this is a valid user session
// go s.readMessages(userSession) // Start reading messages for the user session in a separate goroutine
//
// This function should be called as a goroutine for each user session to ensure that messages
// are processed concurrently and in real-time.
func (s *streamline) readMessages(userSession *userSession) {
	for {

		// Use a channel to signal when the goroutine should exit
		exitChan := make(chan struct{})

		// Listen for the exit signal
		select {
		case <-exitChan:
			// Exit the goroutine
			return
		default:

			_, readMsg, err := userSession.Conn.ReadMessage()
			if err != nil {
				// Break the loop if an error occurs (e.g., the WebSocket connection is closed)
				break
			}

			var msg message
			if err := json.Unmarshal(readMsg, &msg); err != nil {
				// Skip the message if it cannot be unmarshaled into a message struct
				continue
			}

			// Apply all dynamic filters
			for _, filter := range s.dynamicFilters {
				if !filter.Match(msg) {
					// Skip the message if it fails any dynamic filter
					continue
				}
			}

			// Send the message to the queue for processing
			s.messageQueue <- msg

			// Forward message to API (if configured)
			if err := s.forwardMessageToAPI(msg); err != nil {
				// Optionally handle errors from forwarding the message to the API
				continue
			}
		}

	}
}

// processMessages processes messages that have been added to the message queue.
// This function continuously reads messages from the queue and applies any necessary
// processing, such as forwarding messages to an external API. It's designed to run
// continuously, processing messages as they arrive.
//
// The function operates in a loop, continuously reading messages from the queue.
// For each message, it checks for a shutdown signal and exits if received. This
// ensures that the function can be stopped gracefully when the server is shutting down.
//
// If a message is successfully processed, it is forwarded to the external API (if configured).
// If an error occurs during message processing (such as an issue with the external API),
// the function logs the error and continues processing the next message.
//
// Example usage:
// s := NewStreamline(...)
// go s.processMessages() // Start processing messages in a separate goroutine
//
// This function should be called as a goroutine to ensure that messages are processed
// concurrently and in real-time.
func (s *streamline) processMessages() {
	for msg := range s.messageQueue {
		// Process the message
		// Check for shutdown signal and exit if received
		select {
		case <-s.shutdownSignal:
			return

		default:
			// Process the message
			if err := s.forwardMessageToAPI(msg); err != nil {
				// Optionally handle errors from forwarding the message to the API
				continue
			}
		}
	}
}

// writeMessages writes messages to the WebSocket connection associated with the given user session.
// This function continuously reads messages from the user's send channel and writes them
// to the WebSocket connection. It operates in a loop, continuously sending messages
// until an error occurs (such as the WebSocket connection being closed).
//
// The function handles errors that occur during message writing, such as issues with the WebSocket
// connection. In such cases, the function breaks out of the loop, effectively ending the
// message writing process for the current user session.
//
// Example usage:
// userSession := &userSession{...} // Assume this is a valid user session
// go s.writeMessages(userSession) // Start writing messages for the user session in a separate goroutine
//
// This function should be called as a goroutine for each user session to ensure that messages
// are sent concurrently and in real-time.
func (s *streamline) writeMessages(userSession *userSession) {
	for msg := range userSession.SendChan {
		if err := userSession.Conn.WriteMessage(websocket.TextMessage, msg); err != nil {
			// Break the loop if an error occurs (e.g., the WebSocket connection is closed)
			break
		}
	}
}

// forwardMessageToAPI forwards a message to the specified API.
// This function takes a message, serializes it into JSON format, and sends it
// to an external API using an HTTP POST request. The API endpoint is
// specified by the `messageForwardApiURL` field of the `streamline` struct.
//
// The function handles the creation of the HTTP request, setting of headers, sending
// the request via an HTTP client, and processing the response. If the API
// request fails with a non-200 status code, the function returns an error.
//
// Example usage:
// s := NewStreamline(...)
// msg := message{...} // Assume this is a valid message
//
//	if err := s.forwardMessageToAPI(msg); err != nil {
//	    // Handle the error
//	}
//
// This function is called within the message processing pipeline to forward messages
// to an external API for further processing or storage.
func (s *streamline) forwardMessageToAPI(msg message) error {
	// Serialize the message into JSON format
	jsonData, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	// Create a new HTTP POST request to the API endpoint
	req, err := http.NewRequest("POST", s.messageForwardApiURL, bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	// Set the Content-Type header to indicate that the request body contains JSON data
	req.Header.Set("Content-Type", "application/json")

	// Send the request using an HTTP client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// Check the response status code
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("API request failed with status code %d: %s", resp.StatusCode, string(body))
	}

	// If the API request is successful, return nil to indicate no error
	return nil
}

// sendMessageToUser sends a message to a specific user.
// This function takes a user ID and a message, serializes the message into JSON format,
// and sends it to the user's WebSocket connection. The user's session is
// retrieved from the hub using the provided user ID.
//
// The function handles the serialization of the message and the sending of the
// message to the user's WebSocket connection. If the user is not found in the hub
// or if an error occurs during message serialization or sending, the function
// returns an error.
//
// Example usage:
// s := NewStreamline(...)
// userID := "exampleUserID"
// msg := message{...} // Assume this is a valid message
//
//	if err := s.sendMessageToUser(userID, msg); err != nil {
//	    // Handle the error
//	}
//
// This function is called within the message processing pipeline to send messages
// to specific users. It is also used in the BroadcastMessage method to send messages to all users.
func (s *streamline) sendMessageToUser(userID string, msg message) error {
	// Acquire a read lock to safely access the hub
	s.mu.RLock()
	// Retrieve the user's session from the hub
	userSession, ok := s.hub[userID]
	// Release the read lock
	s.mu.RUnlock()

	// Check if the user was found in the hub
	if !ok {
		return errors.New("user not found")
	}

	// Serialize the message into JSON format
	sendMsg, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	// Send the serialized message to the user's WebSocket connection
	userSession.SendChan <- sendMsg
	return nil
}

// BroadcastMessage broadcasts a message to all connected users.
// This function parses the request body, prepares the message to be broadcasted,
// and sends it to all active user sessions. The message is sent to each user's
// WebSocket connection, allowing for real-time communication with all clients.
//
// The function handles the creation of the message, the serialization of the message,
// and the sending of the message to each user's WebSocket connection. If an error
// occurs during message serialization or sending, the function logs the error
// and continues broadcasting the message to the remaining users.
//
// Example usage:
// s := NewStreamline(...)
// // Assume an HTTP request is received with a message to be broadcasted
// s.BroadcastMessage(w, r)
//
// This function is called in response to an HTTP request to broadcast a message
// to all connected users. It is also used in the message processing pipeline to send messages
// to all users.
func (s *streamline) BroadcastMessage(w http.ResponseWriter, r *http.Request) {
	// Parse the request body
	var request struct {
		Content string `json:"content"`
	}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	// Validate the input
	if request.Content == "" {
		http.Error(w, "Content cannot be empty", http.StatusBadRequest)
		return
	}

	// Prepare the message to be broadcasted
	msg := message{
		EventType: "broadcast",
		Content:   request.Content,
		Receiver:  []string{}, // Empty receiver array since we're broadcasting to all
		Sender:    "system",
	}

	// Collect errors
	var errors []error

	// Broadcast the message to all connected users
	for _, userSession := range s.hub {
		// When logging errors, include more context:
		if err := s.sendMessageToUser(userSession.id, msg); err != nil {
			// Log the error and add it to the errors slice
			log.Printf("Failed to send message to user %s: %v\n", userSession.id, err)
			errors = append(errors, fmt.Errorf("failed to send message to user %s: %v", userSession.id, err))
		}
	}

	// Respond with success or errors
	if len(errors) > 0 {
		// Send errors as an array with user IDs
		errorResponse, _ := json.Marshal(errors)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(errorResponse)
	} else {
		// Respond with success
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Message broadcasted successfully"))
	}
}
