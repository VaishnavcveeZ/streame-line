package main

import (
	"fmt"
	"net/http"
	"os"
	"stream-line/streamline"
	"time"

	"github.com/golang-jwt/jwt"
)

// JWTValidator is a function type that validates JWT tokens and returns the user's unique ID.
type JWTValidator func(tokenString string) (string, error)

// ExampleJWTValidator is a simple example of a JWT validator function.
func ExampleJWTValidator(tokenString string) (string, error) {
	// Parse the token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("your-secret-key"), nil
	})

	if err != nil {
		return "", err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// Extract the user ID from the claims
		userID, ok := claims["userID"].(string)
		if !ok {
			return "", fmt.Errorf("user ID claim not found or not a string")
		}
		return userID, nil
	}

	return "", fmt.Errorf("invalid token")
}

func main() {
	// Initialize the StreamlineAPI with a JWT validator and an API URL for forwarding messages
	s := streamline.NewStreamline(ExampleJWTValidator, "http://example.com/api/messages", 100)

	// In your main.go or wherever you're setting up the filters
	s.AddFilter(streamline.NewEventTypeFilter("allowedEventType", "uniqueFilterID"))

	// Start listening for shutdown signals to gracefully shut down the server
	go s.ListenForShutdownSignals()

	// Set up the HTTP handler for broadcasting messages
	http.HandleFunc("/broadcast", func(w http.ResponseWriter, r *http.Request) {
		s.BroadcastMessage(w, r)
	})

	// Set up the WebSocket handler for upgrading HTTP connections to WebSocket connections
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		s.UpgradeToWebSocket(w, r)
	})

	// Start the HTTP server
	server := &http.Server{
		Addr:         ":8080",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	fmt.Println("Starting server on :8080")
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		fmt.Printf("Server startup failed: %v\n", err)
		os.Exit(1)
	}
}
