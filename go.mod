module stream-line

go 1.21.4

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.1
)

require golang.org/x/net v0.17.0 // indirect
